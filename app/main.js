
  function component2(){
    var ele = document.createElement('div');

    // _.map 是 lodash 的函式
    ele.innerHTML = _.map(['Hello Main','webpack'], function(item){
      return item + ' ';
    });
    console.log('component2')
    return ele;
  }
  
  //window.component2 = component2();
 // global['component2']  = component2;
  //var Main['component2']  = component2;
  //document.body.appendChild(component2());