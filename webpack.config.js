const path = require('path');
// 方法一 
/*module.exports = {
    entry:  './app/index.js',
    output: {
        path: path.resolve("./dist"),
        filename: "bundle.js",
        library: 'rstate',
        libraryTarget:'umd', 
        libraryExport: 'default'
    }
  }*/

// 方法 二
module.exports = {
    entry:  './app/index.js',
    output: {
        path: path.resolve("./dist"),
        filename: "bundle.js",
        library: 'EntryPoint',
        libraryTarget:'var',
        libraryExport: 'default'
    }
  }